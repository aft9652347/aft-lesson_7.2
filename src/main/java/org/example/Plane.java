package org.example;

import java.util.Objects;

public class Plane {
    static int flightHours;
    static int numberAircraftProduced;

    String name;
    int loadCapacity;
    int maxSpeed;
    int numberOfEngines;

    public Plane(String name, int loadCapacity, int speed, int numberOfEngines){
        this.name = name;
        this.loadCapacity = loadCapacity;
        this.maxSpeed = speed;
        this.numberOfEngines = numberOfEngines;
        numberAircraftProduced++;
    }


    @Override
    public String toString() {
        return "Plane{" +
                "name='" + name + '\'' +
                ", loadCapacity=" + loadCapacity +
                ", maxSpeed=" + maxSpeed +
                ", numberOfEngines=" + numberOfEngines +
                '}';
    }
    @Override
    public boolean equals(Object o){
        if (o==null) return false;
        if (this == o || getClass() != o.getClass()) return false;
        Plane standard = (Plane) o;
        return
                this.name.equals(standard.name)
                && this.loadCapacity == standard.loadCapacity
                && this.maxSpeed == standard.maxSpeed
                && this.numberOfEngines == standard.numberOfEngines;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Plane plane = (Plane) o;
//        return loadCapacity == plane.loadCapacity && maxSpeed == plane.maxSpeed && numberOfEngines == plane.numberOfEngines && Objects.equals(name, plane.name);
//    }

    @Override
    public int hashCode() {
        return Objects.hash(name, loadCapacity, maxSpeed, numberOfEngines);
    }
}
