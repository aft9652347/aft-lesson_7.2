package org.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Plane plane1 = new Plane("Ил-76",60000, 900, 4);
        Plane plane2 = new Plane("Ил-76",60000, 900, 4);
        Plane plane3 = new Plane("Су-57", 7500, 2600, 2);

        System.out.println(plane1);
        System.out.println(plane2);
        System.out.println(plane3);
        System.out.println(plane1.equals(plane2));
        System.out.println(plane1.equals(plane3));
        System.out.println(plane1.equals(null));
        System.out.println(plane1.equals("sqsqw12w"));
        System.out.println(Plane.numberAircraftProduced);

        System.out.println(plane1.hashCode());
        System.out.println(plane2.hashCode());
        System.out.println(plane3.hashCode());





    }
}